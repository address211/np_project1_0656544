#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#define MAXLINE 10000
#define SERV_TCP_PORT 10110
#define MAX_CMDNUMBER 4000
#define PATHSIZE 1024
#define BUFFSIZE 10000

int pipetable[1000][2]={{0}};

// fprintf(stderr,"%s",DEBUG);

void sig_handle(int sig){
	int stat;
	switch(sig){
		case 17:
		waitpid(0,&stat,WNOHANG);
		break;
		default:
		break;
	}
}

// initial env
void initenv(){
	char path[PATHSIZE];
	getcwd(path,PATHSIZE);
	strcat(path,"/ras");
	chdir(path);
	chroot(path);
	setenv("PATH","bin:.",1);
}

void profix(int sockfd){
	char *pro = "% ";
	write(sockfd,pro,2);
}

int err_dump(char* p){
	perror(p);
	exit(1);
}

//	readline also cut blank

int readline(int fd,char *ptr, int maxlen){
	int n, rc;
	char c;
	for(n=1;n<maxlen; n++){
		if ((rc=read(fd, &c, 1))==1){
			if (c == '\r')
				continue;
			if (c == '\n')
				break;
			*ptr++ = c;
		}else if(rc==0){
			if(n==1) return(0);
			else break;
		}else
			return(-1);
	}
	*ptr = '\0';
	return(n);
}

// cut the line

int cutline(char *line, char *cmd[], char *cut){
	if(!line) return 0;
	char *str = NULL;
	char *ptr = NULL;
	char *dup = strdup(line);

	str = strtok_r(dup, cut, &ptr);
	if (str == NULL){
		err_dump("error : client no input\n");
		free(dup);
		return -1;
	}
	int count =0;
	do{
		cmd[count]=strdup(str);
		str = strtok_r(NULL, cut, &ptr);
		count++;
	}while(str);
	cmd[count]=NULL;
	free(dup);
	return count;
}
// environment configure

void printenv(int sockfd,char *cmd[]){
	if (cmd[1] != NULL){
		char *text;
		text = malloc((sizeof(char))*(BUFFSIZE));
		sprintf(text,"%s=%s\n",cmd[1],getenv(cmd[1]));
		write(sockfd,text,strlen(text));
		free(text);
	}
}

void setenv_cmd(int sockfd, char *cmd[]){
	if (cmd[2]!=NULL){
		setenv(cmd[1],cmd[2],1);
		char *text;
		text = malloc((sizeof(char))*(BUFFSIZE));
		// buy DLC to unlock
		//sprintf(text,"setenv %s=%s\n",cmd[1],cmd[2]);
		//write(sockfd,text,strlen(text));
		free(text);
	}
}

void tableclock(){
	for(int i=0;i<1000;i++){
		if(pipetable[i][0]>0){
			if(pipetable[i][0]==1){
				pipetable[i][0]=-1;
				dup2(pipetable[i][1],0);
			}
			else pipetable[i][0]--;
		}
	}
}

void pluspipe(){
	for(int i=0;i<1000;i++){
		if(pipetable[i][0]>0){
				pipetable[i][0]++;
			}
			else if(pipetable[i][0]==-1) pipetable[i][0]=1;
		}
}

void closepipe(){
	for(int i=0;i<1000;i++){
		if(pipetable[i][0]==-1){
			pipetable[i][0]=0;
			close(pipetable[i][1]);
		}
	}
}

void createpipe(int pn,int fd0,int fd1){
	int insert=-1;
	for(int i=0;i<1000;i++){
		if(pipetable[i][0]==0){
			pipetable[i][1]=fd0;
			pipetable[i][0]=pn;
			insert = i;
			break;
		}
	}
	if(insert==-1) err_dump("pipetable full");
	int len=0;
	char *buff;
	buff=malloc((BUFFSIZE+1)*sizeof(char));
	for(int i=0;i<1000;i++){
		if(i==insert) continue;
		else if(pipetable[i][0]==pn){
			while((len=read(pipetable[i][1],buff,BUFFSIZE))){
			buff[len]=0;
			write(fd1,buff,strlen(buff));
			}
			close(pipetable[i][1]);
			pipetable[i][0]=0;
			break;
		}
	}
	free(buff);
}

// check pipe

int classify_X(char *cmd[],int count){
	for(int i=0;i<count;i++){
		if (cmd[i][0]=='|') return 1;
		if (cmd[i][0]=='>') return 2;
	}
	return 0;
}

int returnX(char *cmd[],int count){
	for(int i=0;i<count;i++){
		if (cmd[i][0]=='|') return i;
		if (cmd[i][0]=='>') return i;
	}
	return 0;
}

int splitbyX(char* front[],char* cmd[],int pn,int cn){
  char* t[MAX_CMDNUMBER];
  for(int i=0;i<cn;i++){
    t[i]=strdup(cmd[i]);
  }
  for(int i=0;i<cn+1;i++){
    free(cmd[i]);
  }
  for(int i=0;i<pn;i++){
    front[i]=strdup(t[i]);
  }
  front[pn]=0;
  for(int i=pn+1;i<cn;i++){
    cmd[i-pn-1]=strdup(t[i]);
  }
  cmd[cn-pn-1]=0;
  for(int i=0;i<cn;i++){
    free(t[i]);
  }
  return cn-pn-1;
}

void execvpplus(int sockfd,char *cmd[]){
	char *text;
	text = malloc((sizeof(char))*(BUFFSIZE));
	if(execvp(cmd[0],cmd)==-1){
		sprintf(text,"Unknown command: [%s].\n",cmd[0]);
		write(sockfd,text,strlen(text));
		free(text);
		exit(1);
	}
	free(text);
	exit(0);
}

void freecmd(char *cmd[],int count){
	for(int i=0;i<count;i++)
	free(cmd[i]);
}

int pipe_cmd(char *front[],char *cmd[],int sockfd,int cmd_number){
	int fd[2],pipetag,childpid,status=0,pipeN=0;
	pipe(fd);
	pipetag=returnX(cmd,cmd_number);
	if(cmd[pipetag][1]==0) pipeN=1;
	else	pipeN=atoi(&cmd[pipetag][1]);
	createpipe(pipeN,fd[0],fd[1]);
	cmd_number=splitbyX(front,cmd,pipetag,cmd_number);
	if((childpid = fork())<0) err_dump("server:fork error");
	else if(childpid == 0){
		close(fd[0]);
		dup2(fd[1],1);
		execvpplus(sockfd,front);
	}
	else wait(&status);
	close(fd[1]);
	freecmd(front,pipetag+1);
	if(WEXITSTATUS(status)==1){
		freecmd(cmd,cmd_number+1);
		cmd_number=0;
		pluspipe();
	}
	return cmd_number;
}

int write_cmd(char *front[],char *cmd[],int sockfd,int cmd_number){
	int pipetag,childpid,fd[2];
	pipetag=returnX(cmd,cmd_number);
	cmd_number=splitbyX(front,cmd,pipetag,cmd_number);
	FILE *file;
	file=fopen(cmd[0],"w");
	pipe(fd);
	if((childpid = fork())<0) err_dump("server:fork error");
	else if(childpid == 0){
		close(fd[0]);
		dup2(fd[1],1);
		execvpplus(sockfd,front);
	}
	else wait(NULL);
	close(fd[1]);
	char *buff=malloc((BUFFSIZE+1)*sizeof(char));
	int len;
	while((len=read(fd[0],buff,BUFFSIZE))){
	buff[len]=0;
	fprintf(file,"%s",buff);
	}
	close(fd[0]);
	fclose(file);
	free(buff);
	freecmd(front,pipetag+1);
	freecmd(cmd,cmd_number+1);
	cmd_number=0;
	return cmd_number;
}

// str_back

int str_back(int sockfd){
	int num;
	char *line;
	char *cmd[MAX_CMDNUMBER];
	char *front[MAX_CMDNUMBER];
	int tag;
	int childpid;
	int cmd_number;
	int status=0;
	dup2(sockfd,2);
	for(;;){
		line=malloc(MAXLINE*sizeof(char));
		profix(sockfd);
		num = readline(sockfd, line, MAXLINE);
		if(num==0) return 0;
		else if (num<0) err_dump("str_echo:readline error");
		// cut the line
		cmd_number=cutline(line, cmd," ");
		while(cmd_number){
			tableclock();
			tag=classify_X(cmd,cmd_number);
			if (tag==1) cmd_number=pipe_cmd(front,cmd,sockfd,cmd_number);
			else if(tag==2) cmd_number=write_cmd(front,cmd,sockfd,cmd_number);
			else{
				if (strcmp(cmd[0],"exit")==0) exit(0);
				else if (strcmp(cmd[0],"setenv")==0) 	setenv_cmd(sockfd,cmd);
				else if (strcmp(cmd[0],"printenv")==0) printenv(sockfd,cmd);
				else{
					if((childpid = fork())<0) err_dump("server:fork error");
					else if(childpid == 0){
						dup2(sockfd,1);
						execvpplus(sockfd,cmd);
					}
					else wait(&status);
				}
				freecmd(cmd,cmd_number+1);
				cmd_number=0;
			}
			closepipe();
		} //while
		free(line);
	} //for
}	//str_back

//
// MAIN AREA
//

int main(int argc,char *argv[]){
	int	sockfd, newsockfd, childpid;
	struct sockaddr_in	cli_addr, serv_addr;
	char *welcome = "****************************************\n\
** Welcome to the information server. **\n\
****************************************\n";
	if((sockfd = socket(AF_INET, SOCK_STREAM, 0))<0)
		err_dump("server: can't open stream socket");

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(SERV_TCP_PORT);

	int opt=1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,&opt, sizeof(opt));
	if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr))< 0)
		err_dump("server: can't bind local address");
	listen(sockfd, 5);
	initenv();
	signal(SIGCHLD,sig_handle);
	for(;;){
		socklen_t clilen = sizeof(cli_addr);
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		if(newsockfd < 0) err_dump("server: accept error");
		if((childpid = fork())<0) err_dump("server:fork error");
		else if(childpid == 0){
			close(sockfd);
			// welcome
			write(newsockfd,welcome,strlen(welcome));
			str_back(newsockfd);
			exit(0);
		}else{
			close(newsockfd);
		}
	}
}
